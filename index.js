var scraper = require('./lib/scraper'),
  Rank = require('./models/rank'),
  fs = require('fs'),
  mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/stats');
console.log(mongoose.connection.readyState);

var options = {
  keyword: "decoda.com",
  language: "en",
  num: 10,
  tld: "com"
},
  results = null,
  file = __dirname + '/../../../logs/serpRank.json',
  date = new Date(),
  today = date.getYear() + '-' + date.getMonth() + 1 + '-' + date.getDate();

var scrape = new scraper.Scraper(options);

fs.readFile(file, 'utf-8', function (err, contents) {
  if (err) {
    console.log(err);
    return;
  }
  processFile(contents);
});

function processFile(contents) {
  contents = JSON.parse(contents);
  
  contents.forEach(function (el, index) {
    Rank.find({
      keyword_id: el.keyword_id,
      user_id: el.user_id,
      date: today
    }, function (err, rank) {
      if (err) {
        throw err;
      }

      // no record found for today
      if (rank.length === 0) {
        findRank(el);
        console.log("No record for " + el.keyword + " on date => " + today);
      }
    });
  });
}

function findRank(el) {
  /*try {
    scrape.getSerps(function (results) {
      results = results;
      console.log(results);
    });
  } catch (ex) {
    console(ex);
  }*/
}
